// Game Values
let min = 1,
    max = 10,
    winningNum = getRandomNum(min, max),
    guessesLeft = 3

// UI Elements
const game = document.querySelector('#game'),
    minNum = document.querySelector('.min-num'),
    maxnum = document.querySelector('.max-num'),
    guessBtn = document.querySelector('#guess-btn'),
    guessInput = document.querySelector('#guess-input'),
    message = document.querySelector('.message')

// Assign UI min. max
minNum.textContent = min
maxnum.textContent = max

// PLay again event listener

game.addEventListener('mousedown', function(e) {
    if(e.target.className === 'play-again') {
        window.location.reload()
    }
}) 

//Listen for guess
guessBtn.addEventListener('click', function() {
    let guess = parseInt(guessInput.value)
    if(isNaN(guess) || guess < min || guess > max) {
        setMessage(`Please Enter a Number Between ${min} and ${max}`, 'red')
    } else {
        setMessage('')
    }

    if(guess === winningNum) {
        // // disabled input
        // guessInput.disabled = true
        // // change border color
        // guessInput.style.borderColor = 'green'
        // //Set message
        // setMessage(`${winningNum} is correct, YOU WIN!`, 'green')

        gameOver(true, `${winningNum} is correct, YOU WIN!`)
    } else {
        // wrong number
        guessesLeft -= 1

        if(guessesLeft === 0) {
            // //game over
            // // disabled input
            // guessInput.disabled = true
            // // change border color
            // guessInput.style.borderColor = 'red'
            // //Set message
            // setMessage(`GAME OVER, you lost! The correct number was ${winningNum}`, 'red')
            gameOver(false, `GAME OVER, you lost! The correct number was ${winningNum}`)
        } else {
            //game continoues - answer wrong
            // change border color
            guessInput.style.borderColor = 'red'
            setMessage(`${guess} is not correct, ${guessesLeft} guesses left`, 'red')

            guessInput.value = ''
        }
    }
})

// SetMessage
function setMessage(msg, color) {
    message.style.color = color
    message.textContent = msg
}

function gameOver(won, msg) {
    let color
    won === true ? color = 'green' : 'red'


    // disabled input
    guessInput.disabled = true
    // change border color
    guessInput.style.borderColor = color

    message.style.color = color
    //Set message
    setMessage(msg)

    // Play again

    guessBtn.value = 'Play Again'
    guessBtn.className += 'play-again'
}

function getRandomNum(min, max) {
    return Math.floor(Math.random()*(max-min+1)+min)
}
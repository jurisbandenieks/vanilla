const http = new EasyHTTP

// Get users
// http.get('https://jsonplaceholder.typicode.com/users')
// .then(data => console.log(data))
// .catch(err => console.log(err))

// Post users
// const data = {
//     name: 'John Doe',
//     username: 'JohnDoe',
//     email: 'jdoe@gmail.com'
// }

// http.post('https://jsonplaceholder.typicode.com/users', data)
// .then(data => console.log(data))
// .catch(err => console.log(err))

// Put user
// const data = {
//     name: 'John Doe',
//     username: 'JohnDoe',
//     email: 'jdoe@gmail.com'
// }

// http.put('https://jsonplaceholder.typicode.com/users/1', data)
// .then(data => console.log(data))
// .catch(err => console.log(err))

// Delete user
http.delete('https://jsonplaceholder.typicode.com/users/1')
.then(data => console.log(data))
.catch(err => console.log(err))
class UI {
    constructor() {
        this.profile = document.getElementById('profile')
    }

    showProfile(user) {
        this.profile.innerHTML = 
        `
        <div class="card card-body mb-3">
            <div class="row">
                <div class="col-md-3">
                    <img class="img-fluid mb-2" src="${user.avatar_url}">
                    <a href="${user.html_url}" target="_blank" class="btn btn-primary btn-block">View Profile</a>
                </div>
                <div class="col-md-9">
                    <span class="badge badge-primary">Public repos: ${user.public_repos}</span>
                    <span class="badge badge-secondary">Public gists: ${user.public_gists}</span>
                    <span class="badge badge-success">Followers: ${user.folower}</span>
                    <span class="badge badge-info">Following: ${user.following}</span>
                    <br><br>
                    <ul class="list-group">
                        <li class="list-group-item">Company: ${user.company}</li>
                        <li class="list-group-item">Webiste/Blog: ${user.blog}</li>
                        <li class="list-group-item">Location: ${user.location}</li>
                        <li class="list-group-item">Member since: ${user.create_at}</li>
                    </ul>
                </div>
            </div>
        </div>
        <h3 class="page-heading mb-3">Latest repos</h3>
        <div id="repos"></div>
        `
    }

    showRepos(repos) {
        let output = ''

        repos.forEach(function(repo) {
            output += 
        `
        <div class="card card-body mb-2">
            <div class="row">
                <div class="col-md-6">
                    <a href="${repo.html_url}" target="_blank">${repo.name}</a>
                </div>
                <div class="col-md-6">
                    <span class="badge badge-primary">Stars: ${repo.stargazers_count}</span>
                    <span class="badge badge-secondary">Watchers: ${repo.watchers}</span>
                    <span class="badge badge-success">Forks: ${repo.forks_count}</span>
                </div>
            </div>
        </div>
        `
        })

        // Outpu repos
        document.getElementById('repos').innerHTML = output
    }

    // Clear profile section
    clearProfile() {
        this.profile.innerHTML = ''
    }

    // Clear alert message
    clearAllert() {
        const currentAllert = document.querySelector('.alert')

        if(currentAllert) {
            currentAllert.remove()
        }
    }

    // Show allert
    showAllert(message, className) {
        this.clearAllert()
        // Create div
        const div = document.createElement('div')
        // Add class name
        div.className = className
        div.appendChild(document.createTextNode(message))
        // Get a parent
        const container = document.querySelector('.searchContainer')
        // Get searchbox
        const search = document.querySelector('.search')
        // Insert allert
        container.insertBefore(div, search)

        // Timeout after 3s
        setTimeout(() => {
            this.clearAllert()
        }, 3000)
    }
}
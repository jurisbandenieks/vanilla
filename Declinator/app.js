// UI elements
const form = document.querySelector('#input-fields')
const name = document.querySelector('#name-input')
const gender = document.querySelector('#gender-value')
const declination = document.querySelector('#declination-value')
const genderAndDeclination = document.querySelector('#gender-and-declination')
const table = document.querySelector('#table')

// Declination table elements
const nomSing = document.querySelector('#nomSing')
const nomPlur = document.querySelector('#nomPlur')
const genSing = document.querySelector('#genSing')
const genPlur = document.querySelector('#genPlur')
const datSing = document.querySelector('#datSing')
const datPlur = document.querySelector('#datPlur')
const akkSing = document.querySelector('#akkSing')
const akkPlur = document.querySelector('#akkPlur')
const infSing = document.querySelector('#infSing')
const infPlur = document.querySelector('#infPlur')
const lokSing = document.querySelector('#lokSing')
const lokPlur = document.querySelector('#lokPlur')
const vokSing = document.querySelector('#vokSing')
const vokPlur = document.querySelector('#vokPlur')

// Hide UI elements before input
genderAndDeclination.style.display = 'none'
table.style.display = 'none'

loadEventListeners()
// On Submit launch declination
function loadEventListeners() {
    form.addEventListener('submit', declinate)
}

// Main function
function declinate(e) {
    const userInput = name.value.trim()    // User inputed value
    const lastLetter =  userInput.slice(-1) // Gets last letter from input
    let base = userInput.substring(0, userInput.length - 1) // Gets name minus last leter

    const exceptions = [
        'mēness', 'akmens', 'asmens', 'rudens', 'ūdens', 'zibens', 'suns', 'sāls'
    ] // Second declination exceptions

    const sixthDeclination = ['acs', 'asins', 'ass', 'auss', 'avs', 'balss', 'brokastis', 'dakts', 'debess',
    'duris', 'durvis', 'dzelzs', 'govs', 'guns', 'haizivs', 'krāsns', 'krūts', 'maksts', 'nakts',
    'nots', 'nāss', 'pils', 'pirts', 'sirds', 'smiltis', 'sāls', 'tāss', 'uguns', 'uts', 'valsts', 'vantis',
    'zivs', 'zoss', 'ģints'] // Sixth declination

    if(name.value === '') {
        alert('Input a Verb!')
    }

    const isSixthDeclination = sixthDeclination.indexOf(userInput) // Checks if input is sixth declination
    const isException = exceptions.indexOf(userInput) // Checks if input is second declination exception

    if (isSixthDeclination !== -1) {    // 6h Declination
        nomSing.textContent = base + 's'
        genSing.textContent = base + 's'
        datSing.textContent = base + 'ij'
        akkSing.textContent = base + 'i'
        infSing.textContent = 'ar ' + base + 'i'
        lokSing.textContent = base + 'ī' 
        vokSing.textContent = base + 's'
        nomPlur.textContent = base + 'is'
        genPlur.textContent = pluralSixthDeclinationGenetive(base) + 'u'
        datPlur.textContent = base + 'īm'
        akkPlur.textContent = base + 'is'
        infPlur.textContent = 'ar ' + base + 'īm'
        lokPlur.textContent = base + 'īs'
        vokPlur.textContent = base + 'is'

        gender.textContent = 'Feminine'
        declination.textContent = 'sixth'

        table.style.display = 'block'
        genderAndDeclination.style.display = 'block'
    } else {
        if(isException !== -1) {    // 2nd Declination exceptions
            nomSing.textContent = base + 's'
            genSing.textContent = base + + 's'
            datSing.textContent = base + 'im'
            akkSing.textContent = base + 'i'
            infSing.textContent = 'ar ' + base + 'i'
            lokSing.textContent = base + 'ī' 
            vokSing.textContent = base + ''
            nomPlur.textContent = singularSecondDeclinationGenetiveAndPlural(base) + 'i'
            genPlur.textContent = singularSecondDeclinationGenetiveAndPlural(base) + 'u'
            datPlur.textContent = 'ar ' + singularSecondDeclinationGenetiveAndPlural(base) + 'iem'
            akkPlur.textContent = singularSecondDeclinationGenetiveAndPlural(base) + 'us'
            infPlur.textContent = singularSecondDeclinationGenetiveAndPlural(base) + 'iem'
            lokPlur.textContent = singularSecondDeclinationGenetiveAndPlural(base) + 'os'
            vokPlur.textContent = singularSecondDeclinationGenetiveAndPlural(base) + 'i'

            gender.textContent = 'Masculine'
            declination.textContent = 'second'
            table.style.display = 'block'
            genderAndDeclination.style.display = 'block'
        } else {
            switch(lastLetter) {
                case 's':
                    const secondLetter = base.slice(-1)
                    switch(secondLetter) {
                        case 'i':   // 2nd Declination
                            base = base.substring(0, base.length - 1)
                            nomSing.textContent = base + 'is'
                            genSing.textContent = singularSecondDeclinationGenetiveAndPlural(base) + 'a'
                            datSing.textContent = base + 'im'
                            akkSing.textContent = base + 'i'
                            infSing.textContent = 'ar ' + base + 'i'
                            lokSing.textContent = base + 'ī' 
                            vokSing.textContent = base + 'i'
                            nomPlur.textContent = singularSecondDeclinationGenetiveAndPlural(base) + 'i'
                            genPlur.textContent = singularSecondDeclinationGenetiveAndPlural(base) + 'u'
                            datPlur.textContent = singularSecondDeclinationGenetiveAndPlural(base) + 'iem'
                            akkPlur.textContent = singularSecondDeclinationGenetiveAndPlural(base) + 'us'
                            infPlur.textContent = 'ar ' + singularSecondDeclinationGenetiveAndPlural(base) + 'iem'
                            lokPlur.textContent = singularSecondDeclinationGenetiveAndPlural(base) + 'os'
                            vokPlur.textContent = singularSecondDeclinationGenetiveAndPlural(base) + 'i'
        
                            gender.textContent = 'Masculine'
                            declination.textContent = 'second'
                            table.style.display = 'block'
                            genderAndDeclination.style.display = 'block'
                        break
                        case 'u':   // 3rd Declination
                            base = base.substring(0, base.length - 1)
                            nomSing.textContent = base + 'us'
                            genSing.textContent = base + 'us'
                            datSing.textContent = base + 'um'
                            akkSing.textContent = base + 'u'
                            infSing.textContent = 'ar ' + base + 'u'
                            lokSing.textContent = base + 'ū' 
                            vokSing.textContent = base + 'us'
        
                            gender.textContent = 'Masculine'
                            declination.textContent = 'third'
                            genderAndDeclination.style.display = 'block'
        
                            firstDeclination(base, false, false)
                        break
                        default:    // 1st Declination
                            firstDeclination(base, true, true)
                        break
                    }
                break
                case 'š':   // 1st Declination
                    firstDeclination(base, false, false)
                break
                case 'a':   // 4th Declination
                    nomSing.textContent = base + 'a'
                    genSing.textContent = base + 'as'
                    datSing.textContent = base + 'ai'
                    akkSing.textContent = base + 'u'
                    infSing.textContent = 'ar ' + base + 'u'
                    lokSing.textContent = base + 'ā' 
                    vokSing.textContent = base + 'a'
                    nomPlur.textContent = base + 'as'
                    genPlur.textContent = base + 'u'
                    datPlur.textContent = base + 'ām'
                    akkPlur.textContent = base + 'as'
                    infPlur.textContent = 'ar ' + base + 'ām'
                    lokPlur.textContent = base + 'ās'
                    vokPlur.textContent = base + 'as'
        
                    gender.textContent = 'Feminine'
                    declination.textContent = 'fourth'
        
                    table.style.display = 'block'
                    genderAndDeclination.style.display = 'block'
                break
                case 'e':   // 5th Declination
                    nomSing.textContent = base + 'e'
                    genSing.textContent = base + 'es'
                    datSing.textContent = base + 'ei'
                    akkSing.textContent = base + 'i'
                    infSing.textContent = 'ar ' + base + 'i'
                    lokSing.textContent = base + 'ē' 
                    vokSing.textContent = base + 'e'
                    nomPlur.textContent = base + 'es'
                    genPlur.textContent = pluralFifthDeclinationGenetive(base) + 'u'
                    datPlur.textContent = base + 'ēm'
                    akkPlur.textContent = base + 'es'
                    infPlur.textContent = 'ar ' + base + 'ēm'
                    lokPlur.textContent = base + 'ēs'
                    vokPlur.textContent = base + 'es'
        
                    gender.textContent = 'Feminine'
                    declination.textContent = 'fifth'
        
                    table.style.display = 'block'
                    genderAndDeclination.style.display = 'block'
                break
                default:
                    alert('Input valid Noun')
                break
            }
        }
    }

    e.preventDefault();
}

// First declination Columns
function firstDeclination(base, end, singular) {
    if(singular) {
        nomSing.textContent = end ? base + 's' : base + 'š'
        genSing.textContent = base + 'a'
        datSing.textContent = base + 'am'
        akkSing.textContent = base + 'u'
        infSing.textContent = 'ar ' + base + 'u'
        lokSing.textContent = base + 'ā'
        vokSing.textContent = end ? base + 's' : base + 'š'

        gender.textContent = 'Masculine'
        declination.textContent = 'first'

        table.style.display = 'block'
        genderAndDeclination.style.display = 'block'
    }
    
    nomPlur.textContent = base + 'i'
    genPlur.textContent = base + 'u'
    datPlur.textContent = base + 'iem'
    akkPlur.textContent = base + 'us'
    infPlur.textContent = 'ar ' + base + 'iem'
    lokPlur.textContent = base + 'os'
    vokPlur.textContent = base + 'i'
    table.style.display = 'block'
}

// Consonant change for 2nd declination
function singularSecondDeclinationGenetiveAndPlural(transform) {
    const excpetions = ['tētis', 'viesis']
    const endings = ['astis', 'jis', 'ķis', 'ģis', 'ris', 'skatis']

    for(let i in endings) {
        if((transform + 'is').endsWith(endings[i])) {
            return transform
        }
    }

    const isException = excpetions.indexOf(transform + 'is')

    if(isException !== -1) {
        return transform
    }

    return changes(transform)
}

// Consonant change for 5th declination
function pluralFifthDeclinationGenetive(transform) {
    const lsttt = transform.slice(-3)
    const baseOfTransformThree = transform.substring(0, transform.length - 3)
    const excpetions = ['apaļmute', 'apšaude', 'balamute', 'balle', 'bāze', 'bise',
        'bote', 'brīze', 'flote', 'fronte', 'gāze', 'gide', 'kase', 'kušete', 'mise',
        'mute', 'pase', 'piešaude', 'planšete', 'rase', 'sarakste', 'šprote', 'takse', 'tirāde']
    const endings = ['aste', 'fe', 'ģe', 'ķe', 'mate', 'pēde']

    for(let i in endings) {
        if((transform + 'e').endsWith(endings[i])) {
            return transform
        }
    }
    const isException = excpetions.indexOf(transform + 'e')

    if(isException !== -1) {
        return transform
    }
    
    if(lsttt === 'kst') {
        return baseOfTransformThree + 'kš'
    } else {
        return changes(transform)
    }
}

// Consonant change for 6th declination
function pluralSixthDeclinationGenetive(transform) {
    const excpetions = ['acs', 'aktis', 'ass', 'auss', 'balss', 'brokastis', 'Cēsis', 'dakts', 'debess', 'dzelzs', 'kūts',
        'maksts', 'pirts', 'šalts', 'takts', 'uts', 'uzacs', 'valsts', 'vēsts', 'zoss', 'žults']

    const isException = excpetions.indexOf(transform + 'e')

    if(isException !== -1) {
        return transform
    }
    
    return changes(transform)
}

// Consonant change function for 2nd 5th and 6th declination
function changes(change) {
    const lst = change.slice(-1)
    const lstt = change.slice(-2)
    const baseOfTransform = change.substring(0, change.length - 1)
    const baseOfTransformTwo = change.substring(0, change.length - 2)

    switch(lstt) {
        case 'sn': return baseOfTransformTwo + 'šņ'
        case 'zn': return baseOfTransformTwo + 'žņ'
        case 'sl': return baseOfTransformTwo + 'šļ'
        case 'zl': return baseOfTransformTwo + 'žļ'
        case 'ln': return baseOfTransformTwo + 'ļņ'
        case 'dz': return baseOfTransform + 'dž'
        default:
            switch(lst) {
                case 'b': return baseOfTransform + 'bj'
                case 'm': return baseOfTransform + 'mj'
                case 'p': return baseOfTransform + 'pj'
                case 'v': return baseOfTransform + 'vj'
                case 't': return baseOfTransform + 'š'
                case 'd': return baseOfTransform + 'ž'
                case 'c': return baseOfTransform + 'č'
                case 's': return baseOfTransform + 'š'
                case 'z': return baseOfTransform + 'ž'
                case 'n': return baseOfTransform + 'ņ'
                case 'l': return baseOfTransform + 'ļ'
                default: return transform
            }
    }
}
const http = new easyHTTP

// Get Posts
// http.get('https://jsonplaceholder.typicode.com/posts', function(err, response) {
//     if(err) {
//         console.log(err)
//     } else {
//         console.log(response)
//     }
// })

// Get Single Post
// http.get('https://jsonplaceholder.typicode.com/posts/1', function(err, response) {
//     if(err) {
//         console.log(err)
//     } else {
//         console.log(response)
//     }
// })

// Post Posts
// const data = {
//     title: 'Custom post',
//     body: 'This is a custom post'
// }

// http.post('https://jsonplaceholder.typicode.com/posts', data, function(err, response) {
//     if(err) {
//         console.log(err)
//     } else {
//         console.log(response)
//     }
// })

// Put Post
// const data = {
//     title: 'Custom post',
//     body: 'This is a custom post'
// }

// const id = 1

// http.put(`https://jsonplaceholder.typicode.com/posts/${id}`, data, function(err, response) {
//     if(err) {
//         console.log(err)
//     } else {
//         console.log(response)
//     }
// })

// Delete Posts
const id = 1

http.delete(`https://jsonplaceholder.typicode.com/posts/${id}`, function(err, response) {
    if(err) {
        console.log(err)
    } else {
        console.log(response)
    }
})
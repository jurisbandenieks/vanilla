const listItems = [
  'Item 1',
  'Item 2',
  'Item 3',
  'Item 4',
  'Item 5',
  'Item 6',
  'Item 7',
  'Item 8',
  'Item 9',
  'Item 10',
  'Item 11',
  'Item 12',
  'Item 13',
  'Item 14',
  'Item 15',
  'Item 16',
  'Item 17',
  'Item 18',
  'Item 19',
  'Item 20',
  'Item 21',
  'Item 22',
  'Item 23',
  'Item 24'
]

const listElement = document.getElementById('list')
const paginationElement = document.getElementById('pagination')

let currentPage = 1
let rows = 5

displayList(listItems, listElement, rows, currentPage)
setupPagination(listItems, paginationElement, rows)

function displayList(items, wrapper, rows, page) {
  wrapper.innerHTML = ''
  page--

  let start = rows * page
  let end = start + rows
  let paginatedItems = items.slice(start, end)

  for (let i = 0; i < paginatedItems.length; i++) {
    let item_element = document.createElement('div')
    item_element.classList.add('item')
    item_element.innerText = paginatedItems[i]

    wrapper.appendChild(item_element)
  }
}

function setupPagination(items, wrapper, rowsPerPage) {
  wrapper.innerHTML = ''

  let pageCount = Math.ceil(items.length / rowsPerPage)
  for (let i = 1; i < pageCount + 1; i++) {
    let btn = paginationButton(i, items)
    wrapper.appendChild(btn)
  }
}

function paginationButton(page, items) {
  let button = document.createElement('button')
  button.innerText = page

  if (currentPage == page) {
    button.classList.add('active')
  }

  button.addEventListener('click', function() {
    currentPage = page
    displayList(items, listElement, rows, currentPage)

    let currentBtn = document.querySelector('.page-numbers button.active')
    currentBtn.classList.remove('active')

    button.classList.add('active')
  })

  return button
}

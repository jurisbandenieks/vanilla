// Flag name arrays
const easy = ['argentina', 'australia', 'austria', 'bahamas', 'belarus', 'belgium', 'bolivia', 'brazil', 'bulgaria', 'canada',
'chile', 'china', 'colombia', 'croatia', 'cuba', 'czech-republic', 'denmark', 'egypt', 'finland', 'france', 'germany', 'greece',
'iceland', 'india', 'ireland', 'israel', 'italy', 'japan', 'netherlands', 'new zealand', 'norway', 'poland', 'portugal', 'russia',
'spain', 'sweden', 'switzerland', 'united kingdom', 'united states of america']
const medium = ['afghanistan', 'albania', 'algeria', 'andorra', 'armenia', 'azerbaijan', 'bangladesh', 'bosnia and herzegovina',
'cambodia', 'cameroon', 'costa rica', 'cyprus', 'ecuador', 'estonia', 'ethiopia', 'fiji', 'georgia', 'haiti', 'hungary', 'indonesia',
'iran', 'iraq', 'jamaica', 'kazakhstan', 'kenya', 'kuwait', 'kyrgyzstan', 'latvia', 'lebanon', 'libya', 'liechtenstein', 'lithuania',
'monaco', 'niger', 'nigeria', 'north korea', 'oman', 'pakistan', 'panama', 'papua new guinea', 'paraguay', 'peru', 'philippines',
'qatar', 'romania', 'rwanda', 'san marino', 'saudi arabia', 'serbia', 'singapore', 'slovakia', 'slovenia', 'south africa', 'south korea',
'sri lanka', 'syria', 'taiwan', 'tajikistan', 'tanzania', 'thailand', 'trinitada and tobago', 'tunisia', 'turkey', 'turkmenistan',
'uganda', 'ukrain', 'united arab emirates', 'uruguay', 'uzbekistan', 'vatican city', 'venezuela', 'vietnam', 'zambia', 'zimbabwe']
const hard = ['angola', 'antigua and barbuda', 'bahrain', 'barbados', 'belize', 'benin', 'bhutan', 'botswana', 'brunei', 'burkina faso',
'burundi', 'cape verde', 'central african republic', 'chad', 'comoros', 'congo democratic republic', 'congo republic', 'cote d ivoire', 'djibouti',
'dominica', 'dominican republic', 'east timor', 'el salvador', 'equatorial guinea', 'eritrea', 'gabon', 'gambia', 'ghana', 'grenada',
'guatemala', 'guinea bissau', 'guinea', 'guyana', 'honduras', 'jordan', 'kiribati', 'kosovo', 'laos', 'lesotho', 'liberia', 'nicaragua',
'niue', 'palau', 'saint kitts and nevis', 'samoa', 'sao tome and principe', 'senegal', 'seychelles', 'sierra leone', 'solomon islands',
'somalia', 'south sudan', 'sudan', 'suriname', 'swaziland', 'togo', 'tonga', 'tuvalu', 'yemen']

// UI vars
const game = document.querySelector('#game')
const level = document.querySelectorAll('.level-game')
const flag = document.querySelectorAll('.flag')
const flagOne = document.querySelector('.flag-one')
const flagTwo = document.querySelector('.flag-two')
const flagThree = document.querySelector('.flag-three')
const flagFour = document.querySelector('.flag-four')
let flagName = document.querySelector('.flag-name')



let randomNums = new Array(4)
let gameLevel = ''
// Event listeners
for (let i = 0; i < flag.length; i++)  {
    flag[i].addEventListener("mousedown", function() {
        const flagSrc = flag[i].src
        let flagNameFormatted = flagSrc.slice(flagSrc.lastIndexOf('/')+1, flagSrc.indexOf('flag-medium')-1)
        flagNameFormatted = flagNameFormatted.replace(/-/g, ' ')

        if(flagNameFormatted === flagName.textContent) {
            getFlags()
        }
    });
}

document.addEventListener('DOMContentLoaded', chooseLevel)

function chooseLevel() {
    game.style.display = 'none'
    for (let i = 0; i < level.length; i++)  {
        level[i].addEventListener("click", function() {
            gameLevel = level[i].value

            if(gameLevel) {
                game.style.display = 'block'
                document.querySelector('#level').style.display = 'none'
                getFlags()
            }
        });
    }
}

function getFlags() {
    let i = 0
    while (i < randomNums.length) {
        let randomNum = getRandomFlag(0, easy.length-1)

        let index = randomNums.indexOf(randomNum)
        if(index === -1) {
            randomNums[i]= randomNum
            i++
        }
    }

    // Replece flag string whitespace
    const flagArr = new Array(4)
    switch(gameLevel) {
        case 'easy':
            for (let f = 0; f < flag.length; f++) {
                flagArr[f] = (easy[randomNums[f]]).replace(/ /g, '-')
            }
        break
        case 'medium':
            for (let f = 0; f < flag.length; f++) {
                flagArr[f] = (medium[randomNums[f]]).replace(/ /g, '-')
            }
        break
        case 'hard':
            for (let f = 0; f < flag.length; f++) {
                flagArr[f] = (hard[randomNums[f]]).replace(/ /g, '-')
            }
        break
    }
    

    // Get Flag to guess
    let randomNum = getRandomFlag(0, randomNums.length-1)
    console.log(flagArr)
    flagName.textContent = flagArr[randomNum].replace(/-/g, ' ')
    // Load flags into img

    flagOne.src = `./img/${gameLevel}/${flagArr[0]}-flag-medium.png`
    flagTwo.src = `./img/${gameLevel}/${flagArr[1]}-flag-medium.png`
    flagThree.src = `./img/${gameLevel}/${flagArr[2]}-flag-medium.png`
    flagFour.src = `./img/${gameLevel}/${flagArr[3]}-flag-medium.png`
}

function getRandomFlag(min, max) {
    return Math.floor(Math.random()*(max-min+1)+min)
}
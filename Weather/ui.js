class UI {
    constructor() {
        this.location = document.getElementById('w-location')
        this.desc = document.getElementById('w-desc')
        this.string = document.getElementById('w-string')
        this.details = document.getElementById('w-details')
        this.icon = document.getElementById('w-icon')
        this.humidity = document.getElementById('w-humidity')
        this.feelsLike = document.getElementById('w-feels-like')
        this.dewpoint = document.getElementById('w-dewpoint')
        this.wind = document.getElementById('w-wind')
    }

    paint(loc, weather) {
            this.location.textContent = loc.text
            this.desc.textContent = weather.hourly.summary
            this.string.textContent = weather.currently.temperature
            this.icon.setAttribute('class', 'wi wi-' + weather.currently.icon)
            this.humidity.textContent = `Relative Humidity: ${weather.currently.humidity * 100}%`
            this.feelsLike.textContent = `Feels like: ${weather.currently.apparentTemperature}`
            this.dewpoint.textContent = `Dew point: ${weather.currently.dewPoint}`
            this.wind.textContent = `Wind: ${weather.currently.windSpeed}`        
    }
}
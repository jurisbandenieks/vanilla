class Location {
    constructor(address) {
        this.token = 'pk.eyJ1IjoianVyaXNiYW5kZW5pZWtzIiwiYSI6ImNqeWl6azlzbjA2NjczaHIyY3o0ZjJ1dWMifQ.F7Sfkc25uX0h71OH3cFACQ'
        this.address = address
    }
    // Fetch location
    async getLocation() {
        const response = await fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${this.address}.json?access_token=${this.token}`)

        const responseData = await response.json()

        return responseData.features[0]
    }

    // Change location
    changeLocation(address) {
        this.address = address
    }
}
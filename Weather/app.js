// Init Local storage
const store = new Storage()

// Get stored data
const weatherLocation = store.getLocationData()

// Init location
const loc = new Location(weatherLocation.city)
const ui = new UI()

// Get weather on DOM load
document.addEventListener('DOMContentLoaded', geData)

document.getElementById('w-change-btn').addEventListener('click', (e) => {
    const city = document.getElementById('city').value

    loc.changeLocation(city)

    // Set location
    store.setLocationData(city)

    geData()

    $('#locModal').modal('hide')
})

function geData() {
    loc.getLocation()
    .then(results => {
        const weather = new Weather(results.center[1], results.center[0])
        weather.getWeather()
        .then(data => {
            console.log(results, data)
            ui.paint(results, data)
        })
    })
    .catch(err => console.log(err))
}
